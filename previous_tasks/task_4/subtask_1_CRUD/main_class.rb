# frozen_string_literal: true

require 'English'
class Main
  BUFFER = 'task_4/subtask_1_CRUD/buffer.txt'
  TEXT = 'task_4/subtask_1_CRUD/text.txt'

  def index
    File.read(TEXT)
  end

  def find(ld)
    File.open(TEXT).each do |line|
      puts line if $INPUT_LINE_NUMBER == ld
    end
  end

  def where(pattern)
    File.open(TEXT).each do |line|
      puts line if line.include?(pattern)
    end
  end

  def update(ld, text)
    @file = File.open(BUFFER, 'r+')
    File.open(TEXT).each_with_index do |line, index|
      @file.puts(ld == index ? text : line)
    end
    @file.close
    File.write(TEXT, File.read(BUFFER))
    File.write(BUFFER, '')
  end

  def delete(ld)
    @file = File.open(BUFFER, 'r+')
    File.open(TEXT).each_with_index do |line, index|
      @file.puts(ld == index ? line.clear : line)
    end
    @file.close
    File.write(TEXT, File.read(BUFFER).gsub(/^$\n/, ''))

    File.write(BUFFER, '')
  end
end
