# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'GET /lab_reports', type: :request do
  let(:path) { '/lab_reports' }
  let(:user) { create(:user) }

  context 'when open lab_reports page' do
    it 'successfully open page and render template' do
      sign_in user
      get path
      expect(response).to have_http_status :success
      expect(response).to render_template('index')
    end
  end

  context 'when user not logged in' do
    it 'redirect to log in path' do
      get path
      expect(response).to redirect_to(new_user_session_path)
    end
  end
end
