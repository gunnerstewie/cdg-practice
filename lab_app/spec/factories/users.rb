# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    first_name { 'Alexey' }
    last_name { 'Leskov' }
    sequence(:email) { |n| "#{n}example@gmail.com" }
    password { '123456' }
  end
end
