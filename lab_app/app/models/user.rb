# frozen_string_literal: true

class User < ApplicationRecord
  extend Enumerize

  enumerize :role, in: %i[member manager admin], default: :member
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable, :rememberable, :validatable
  has_many :lab_reports, dependent: :destroy
  validates :first_name, length: { minimum: 1, maximum: 100, allow_blank: false }
  validates :last_name, length: { minimum: 1, maximum: 100, allow_blank: false }
  validates :email, uniqueness: true, length: { minimum: 6, maximum: 100 }
end
