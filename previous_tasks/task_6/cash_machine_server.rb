# frozen_string_literal: true

require 'socket'
require 'rack'
require 'rack/utils'
require_relative './app_class'

server = TCPServer.new('0.0.0.0', 3000)
app = App.new

class CashMachine
  BALANCE = 'task_6/balance.txt'
  BUFFER = 'task_6/buffer.txt'
  def init
    if File.exist?('task_6/balance.txt')
      @balance = 'task_6/balance.txt'
    else
      @balance = File.open('task_6/balance.txt', 'w')
      @balance.write('100.0')
      @balance.close
    end
    File.write(BUFFER, File.read(@balance))
  end

  def open_buffer
    @buffer = File.open(BUFFER, 'r+')
    @cash = File.foreach(BUFFER).first.to_i
  end
end
cash_machine = CashMachine.new
cash_machine.init
cash_machine.open_buffer

while connection = server.accept
  request = connection.gets
  method, full_path = request.split(' ')
  puts full_path
  path = full_path.split('?')[0]
  query_string = full_path.split('?')[1]
  status, headers, body = app.call({
                                     'REQUEST_METHOD' => method,
                                     'PATH_INFO' => path,
                                     'QUERY_STRING' => query_string
                                   })

  connection.print("HTTP/1.1 #{status}\r\n")

  headers.each { |key, value| connection.print("#{key}: #{value}\r\n") }

  connection.print "\r\n"

  body.each { |part| connection.print(part) }

  connection.close
end
