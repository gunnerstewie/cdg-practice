# frozen_string_literal: true

require_relative './greeting'

RSpec.describe '#greeting' do
  describe 'user input' do
    def input_access(word_1, word_2, num)
      allow_any_instance_of(Kernel).to receive(:gets).and_return(word_1, word_2, num)
    end

    context 'when age is greater than 18' do
      before do
        input_access('Alexey', 'Leskov', '20')
      end

      it 'print text for adult users' do
        expect(greeting).to eq('Привет, Alexey Leskov. Самое время заняться делом!')
      end
    end

    context 'when age is lower than 18' do
      before do
        input_access('Alexey', 'Leskov', '17')
      end

      it 'print text for teenager users' do
        expect(greeting).to eq('Привет, Alexey Leskov. Тебе меньше 18 лет, но начать учиться программировать никогда не рано')
      end
    end
  end
end
