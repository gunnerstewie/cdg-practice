# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'POST /users/sign_in', type: :request do
  let(:user) { create(:user) }

  context 'when valid params given' do
    it 'sign in user' do
      sign_in user
      get root_path
      expect(response).to render_template(:index)
    end
  end

  context 'when user not logged in' do
    it 'do not render index page' do
      sign_out user
      get root_path
      expect(response).not_to render_template(:index)
    end
  end
end
