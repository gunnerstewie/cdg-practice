# frozen_string_literal: true

require 'rails_helper'

RSpec.describe LabReport, type: :model do
  it { is_expected.to belong_to(:user) }
  it { is_expected.to accept_nested_attributes_for :user }
  it { is_expected.to validate_length_of(:title).is_at_least(1).is_at_most(250) }
  it { is_expected.to validate_length_of(:description).is_at_most(500) }
end
