# frozen_string_literal: true

def stud_base
  list = 'task_4/subtask_2_stud_base/stud_base_list.txt'
  results = 'task_4/subtask_2_stud_base/results.txt'
  list_data = File.read(list).split(/\n+/)
  file = File.open(results, 'a')
  array = []

  puts 'Enter age for search:'

  while age = gets.chomp
    if (list_data - array.uniq.sort).empty? || age == '-1'
      puts 'program is over'
      file.close
      puts File.read(results)
      File.write(results, '')
      break
    else
      puts 'Enter age for search again please:'
      list_data.each do |line|
        file.puts(line) if line.include?(age)
        array.push(line) if line.include?(age)
      end
    end
  end
end
stud_base
