# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'POST /lab_reports', type: :request do
  let(:path) { '/lab_reports' }
  let(:user) { create(:user) }

  let(:params) { { lab_report: { title: 'Alexey', description: 'Leskov', user_id: user.id } } }

  describe 'when user logged in' do
    before { sign_in user }

    context 'when valid params given' do
      it 'creates new user' do
        post path, params: params
        lab_report = LabReport.last
        expect(response).to have_http_status :found
        expect(response).to redirect_to(lab_reports_path)
        expect(lab_report.title).to eq('Alexey')
        expect(flash[:success]).to match(/Lab report created succesfully!/)
        expect(LabReport.count).to eq(1)
      end
    end

    context 'when title not given' do
      let(:params) { { lab_report: { title: '', description: 'Leskov', user_id: user.id } } }

      it 'redirect to new page' do
        post path, params: params
        expect(response).to have_http_status :success
        expect(response).to render_template('new')
      end
    end

    context 'when user not given' do
      let(:params) { { lab_report: { title: 'Alexey', description: 'Leskov', user_id: nil } } }

      it 'redirect to new page' do
        post path, params: params
        expect(response).to have_http_status :success
        expect(response).to render_template('new')
      end
    end
  end

  context 'when user not logged in' do
    it 'redirect to log in path' do
      post path, params: params
      expect(response).to redirect_to(new_user_session_path)
    end
  end
end
