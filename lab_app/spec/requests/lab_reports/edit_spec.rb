# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'GET /lab_reports/:id/edit', type: :request do
  let(:path) { "/lab_reports/#{lab_report.id}/edit" }

  let(:user) { create(:user) }
  let(:lab_report) { create(:lab_report, user_id: user.id) }

  describe 'when user logged in' do
    before { sign_in user }

    context 'when open edit page for lab_report' do
      it 'open_page' do
        get path
        expect(response).to have_http_status :success
        expect(response).to render_template('edit')
      end
    end

    context 'when do not open edit page for lab_report' do
      let(:path) { '/lab_reports/666/edit' }

      it 'record not found and redirect' do
        get path
        expect(response).to redirect_to(lab_reports_path)
        expect(flash[:danger]).to match(/Record not found/)
      end
    end
  end

  context 'when user not logged in' do
    it 'redirect to log in path' do
      get path
      expect(response).to redirect_to(new_user_session_path)
    end
  end
end
