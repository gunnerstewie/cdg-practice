# frozen_string_literal: true

def string_check
  puts 'Please enter word:'
  string = gets.chomp
  string.end_with?('CS') ? 2**string.length : string.reverse
end
puts string_check
