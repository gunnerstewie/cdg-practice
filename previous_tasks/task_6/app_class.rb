# frozen_string_literal: true

class App
  def call(env)
    balance = File.open('task_6/balance.txt', 'r+')
    buffer = File.open('task_6/buffer.txt', 'r+')
    cash = File.foreach(buffer).first.to_i
    req = Rack::Request.new(env)
    number = req.query_string.split('=')[1].to_i

    case req.path
    when '/balance'
      [200, { 'Content-Type' => 'text/html' }, ["Your current balance is #{File.read(buffer)}"]]
    when '/deposit'
      if number.positive?
        buffer.write(number + cash).to_s
        buffer.close
        [200, { 'Content-Type' => 'text/html' }, ["Your current balance is #{File.read(buffer)}"]]
      else
        [404, { 'Content-Type' => 'text/html' }, ['Invalid parameter!Please input only positive numbers!']]
      end
    when '/withdraw'
      if number.positive? && number <= File.read(buffer).to_i
        buffer.write((cash - number).to_s)
        buffer.close
        [200, { 'Content-Type' => 'text/html' }, ["Your current balance is #{File.read(buffer)}"]]
      else
        [404, { 'Content-Type' => 'text/html' },
         ['Invalid parameter!.Withdraw summary must be positive number and less or equal to your current balance']]
      end
    when '/quit'
      File.write(balance, File.read(buffer))
      File.write(buffer, '')
      abort
    else
      [404, { 'Content-Type' => 'text/html' }, ['Not found']]
    end
  end
end
