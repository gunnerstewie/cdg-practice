# frozen_string_literal: true

class LabReportsController < ApplicationController
  before_action :authenticate_user!

  def index
    @lab_reports = authorized_scope(LabReport.all)
  end

  def new
    @lab_report = LabReport.new
  end

  def create
    @lab_report = LabReport.new(lab_report_params)
    if @lab_report.save
      flash[:success] = 'Lab report created succesfully!'
      redirect_to lab_reports_path
    else
      render 'new'
    end
  end

  def edit
    @lab_report = LabReport.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    flash[:danger] = 'Record not found'
    redirect_to lab_reports_path
  end

  def update
    @lab_report = LabReport.find(params[:id])
    authorize! @lab_report

    if @lab_report.update(lab_report_params)
      flash[:success] = 'Lab report was successfuly updated!'
      redirect_to lab_reports_path
    else
      render 'edit'
    end
  rescue ActionPolicy::Unauthorized
    flash[:danger] = 'You are not authorized to this action'
    redirect_to lab_reports_path
  rescue ActiveRecord::RecordNotFound
    flash[:danger] = 'Record not found'
    redirect_to lab_reports_path
  end

  def destroy
    @lab_report = LabReport.find(params[:id])
    @lab_report.destroy
    flash[:success] = 'Lab report has been destroyed!'
    redirect_to lab_reports_path
  rescue ActiveRecord::RecordNotFound
    flash[:danger] = 'Record not found'
    redirect_to lab_reports_path
  end

  private

  def lab_report_params
    params.require(:lab_report).permit(:title, :description, :grade, :user_id)
  end
end
