# frozen_string_literal: true

FactoryBot.define do
  factory :lab_report do
    title { 'Alexey' }
    description { 'Leskov' }
    user_id { user.id }
  end
end
