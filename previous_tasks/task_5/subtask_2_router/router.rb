# frozen_string_literal: true

require_relative './posts_controller'
require_relative './comments_controller'
require_relative './resource'
class Router
  def initialize
    @routes = {}
  end

  def init
    resources(PostsController, 'posts')
    resources(CommentsController, 'comments')

    loop do
      print 'Choose resource you want to interact (1 - Posts, 2 - Comments, q - Exit): '
      choise = gets.chomp
      case choise
      when '1'
        PostsController.connection(@routes['posts'])
      when '2'
        CommentsController.connection(@routes['comments'])
      when 'q'
        puts 'Good bye!'
        break
      else
        puts 'Input Error! Please enter correct number of resource(1, 2) or q for quit program:'
        redo
      end
    end
  end

  def resources(klass, keyword)
    controller = klass.new
    @routes[keyword] = {
      'GET' => {
        'index' => controller.method(:index),
        'show' => controller.method(:show)
      },
      'POST' => controller.method(:create),
      'PUT' => controller.method(:update),
      'DELETE' => controller.method(:destroy)
    }
  end
end
