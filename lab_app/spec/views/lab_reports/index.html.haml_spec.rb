# frozen_string_literal: true

require 'rails_helper'

RSpec.describe '/lab_reports/index', type: :view do
  context 'with lab_report' do
    let!(:user) { create(:user) }

    before do
      assign(:lab_reports,
        [LabReport.create!(title: 'First', user_id: user.id),
         LabReport.create!(title: 'Second', user_id: user.id)])
    end

    it 'displays lab report' do
      render
      expect(rendered).to match(/First/)
      expect(rendered).to match(/Second/)
    end
  end

  context 'when check controller path' do
    it 'infers the controller path' do
      expect(controller.request.path_parameters[:controller]).to eq('/lab_reports')
      expect(controller.controller_path).to eq('/lab_reports')
    end
  end

  context 'when check controller action' do
    it 'infers the controller action' do
      expect(controller.request.path_parameters[:action]).to eq('index')
    end
  end
end
