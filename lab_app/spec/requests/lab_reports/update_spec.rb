# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'GET /lab_reports/:id', type: :request do
  let(:path) { "/lab_reports/#{lab_report.id}" }

  let(:user) { create(:user) }
  let(:lab_report) { create(:lab_report, user_id: user.id) }
  let(:params) { { lab_report: { grade: 'A' } } }

  describe 'when user logged in' do
    before { sign_in user }

    context 'when update lab_report' do
      it 'successful' do
        put path, params: params
        lab_report = LabReport.last
        expect(response).to have_http_status :found
        expect(response).to redirect_to(lab_reports_path)
        expect(lab_report.grade).to eq('A')
      end
    end

    context 'when wrong params given' do
      let(:path) { '/lab_reports/666' }

      it 'record not found and redirect' do
        put path, params: params
        expect(response).to redirect_to(lab_reports_path)
        expect(flash[:danger]).to match(/Record not found/)
      end
    end
  end

  context 'when user not logged in' do
    it 'redirect to log in path' do
      put path, params: params
      expect(response).to redirect_to(new_user_session_path)
    end
  end
end
