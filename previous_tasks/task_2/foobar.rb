# frozen_string_literal: true

def foobar
  puts 'Please enter first number:'
  number_1 = gets.chomp.to_i
  puts 'Please enter second number:'
  number_2 = gets.chomp.to_i

  number_1 == 20 or number_2 == 20 ? number_2.to_s : (number_1 + number_2).to_s
end
puts foobar
