# frozen_string_literal: true

class LabReportPolicy < ApplicationPolicy
  relation_scope do |scope|
    case user.role
    when 'admin', 'manager'
      scope
    when 'member'
      scope.where(user: user)
    end
  end

  def update?
    record.user == user || user.role == 'admin'
  end
end
