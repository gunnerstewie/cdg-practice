# frozen_string_literal: true

def pokemon_base
  puts 'Please enter number of pokemons:'
  count = gets.chomp.to_i
  keys = %w[name color]
  array = []
  count.times do
    puts 'Please enter name and color of pokemon after coma:'
    values = gets.chomp.split(',')
    array.append(keys.zip(values).to_h)
  end
  pp array
end
pokemon_base
