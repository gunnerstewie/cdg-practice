# frozen_string_literal: true

require 'rack/test'
require_relative './application_class'

RSpec.describe App do
  include Rack::Test::Methods

  describe 'get requests' do
    let(:app) { App.new }
    let(:buffer) { 'task_6/buffer.txt' }

    before(:each) { File.write('task_6/buffer.txt', '200') }

    context 'get to /balance' do
      it 'returns status 200 and message in body of response' do
        get '/balance'
        expect(last_response.status).to eq 200
        expect(last_response.body).to eq 'Your current balance is 200'
      end
    end

    context 'get to /deposit?value=digit' do
      it 'returns status 200 and message in body of response' do
        get '/deposit?value=12'
        expect(last_response.status).to eq 200
        expect(last_response.body).to eq 'Your current balance is 212'
      end
    end

    context 'when given wrong value to deposit request' do
      it 'returns status 404 and message in body of response' do
        get '/deposit?value=dou!'
        expect(last_response.status).to eq 404
        expect(last_response.body).to eq 'Invalid parameter!Please input only positive numbers!'
      end
    end

    context 'get to /withdraw?value=digit' do
      it 'returns status 200 and message in body of response' do
        get '/withdraw?value=12'
        expect(last_response.status).to eq 200
        expect(last_response.body).to eq 'Your current balance is 188'
      end
    end

    context 'when given wrong value to withdraw request' do
      it 'returns status 404 and message in body of response' do
        get '/withdraw?value=dou!'
        expect(last_response.status).to eq 404
        expect(last_response.body).to eq 'Invalid parameter!.Withdraw summary must be positive number and less or equal to your current balance'
      end
    end

    context 'when given wrong request' do
      it 'returns status 404 and message in body of response' do
        get '/dou!'
        expect(last_response.status).to eq 404
        expect(last_response.body).to eq 'Not found'
      end
    end
  end
end
