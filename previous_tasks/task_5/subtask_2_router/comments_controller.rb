# frozen_string_literal: true

require_relative './resource'

class CommentsController
  extend Resource

  def initialize
    @comments = ['packman', 'max payne', 'doom']
  end

  def index
    @comments.each_with_index do |comment, index|
      puts "#{index}.#{comment}"
    end
  end

  def show
    loop do
      puts "Please enter id of comment(begin from 0 to #{@comments.length - 1}): "
      id = gets.chomp
      break if id == 'q'

      if id != '0' && id.to_i.zero?
        puts "Input error! Please enter only numbers(begin from 0 to #{@comments.length - 1}): "
        redo
      elsif @comments[id.to_i].nil?
        puts "Input error!index not exist!enter indeces from 0 to #{@comments.length - 1}: "
        redo
      elsif id.to_i.negative?
        puts "Input error!negative index!enter indeces from 0 to #{@comments.length - 1}: "
        redo
      else
        @comments.each_with_index do |comment, index|
          puts "#{index}.#{comment}" if index == id.to_i
        end
      end
      break
    end
  end

  def create
    puts 'Please enter text of comment: '
    text = gets.chomp
    return if text == 'q'

    @comments << text

    @comments.each_with_index do |comment, index|
      puts "#{index}.#{comment}" if comment == text
    end
  end

  def update
    loop do
      puts "Please enter id of comment to update(begin from 0 to #{@comments.length - 1}): "
      id = gets.chomp
      break if id == 'q'

      if id != '0' && id.to_i.zero?
        puts "Input error! Please enter only numbers(begin from 0 to #{@comments.length - 1}): "
        redo
      elsif @comments[id.to_i].nil?
        puts "Input error!index not exist!enter indeces from 0 to #{@comments.length - 1}: "
        redo
      else
        puts 'Please enter text that update chosen id: '
        text = gets.chomp
        break if text == 'q'

        @comments[id.to_i] = text
        @comments.each_with_index do |comment, index|
          puts "#{index}.#{comment}" if comment == text
        end
      end

      break
    end
  end

  def destroy
    loop do
      puts "Please enter id of comment to delete(begin from 0 to #{@comments.length - 1}): "
      id = gets.chomp
      break if id == 'q'

      if id != '0' && id.to_i.zero?
        puts "Input error! Please enter only numbers(begin from 0 to #{@comments.length - 1}): "
        redo
      elsif @comments[id.to_i].nil?
        puts "Input error!index not exist!enter indeces from 0 to #{@comments.length - 1}: "
        redo
      else
        @comments.delete_at(id.to_i)
      end

      break
    end
  end
end
