# frozen_string_literal: true

require_relative './pokemon_base'

RSpec.describe '#pokemon_base' do
  describe 'user input' do
    def input_access(num)
      allow_any_instance_of(Kernel).to receive(:gets).and_return(num)
    end

    let(:count) { gets.chomp.to_i }

    context 'when user input number of pokemons' do
      before do
        input_access('3')
      end
      it 'user input creates count variable' do
        expect(count).to eq(3)
      end
    end

    context 'when user do not input number of pokemons' do
      before do
        input_access('')
      end
      it 'return empty array' do
        expect(pokemon_base).to eq([])
      end
    end

    context 'when user input name and color of pokemons' do
      before do
        allow_any_instance_of(Kernel).to receive(:gets).and_return('2', 'pikachu,yellow', 'raichu,orange')
      end

      let(:keys) { %w[name color] }
      let(:array) { [] }
      let(:values) { gets.chomp.split(',') }

      it 'creates array of hashes' do
        expect(pokemon_base).to eq([{ 'name' => 'pikachu', 'color' => 'yellow' },
                                    { 'name' => 'raichu', 'color' => 'orange' }])
      end
    end
  end
end
