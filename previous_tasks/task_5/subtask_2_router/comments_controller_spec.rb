# frozen_string_literal: true

require_relative './comments_controller'

RSpec.describe CommentsController do
  subject { described_class.new }

  def input_access(num)
    allow_any_instance_of(Kernel).to receive(:gets).and_return(num)
  end

  describe '#initialize' do
    context 'initializing of comments controller' do
      it 'comment controller is an instance of class' do
        expect(subject).to be_an_instance_of(CommentsController)
      end
    end
  end

  describe '#index' do
    context 'return array of comment with indeces' do
      it 'return array of comments' do
        expect(subject.index).to eq(['packman', 'max payne', 'doom'])
      end

      it 'print each element of array with its index to stdout' do
        expect { subject.index }.to output("0.packman\n1.max payne\n2.doom\n").to_stdout
      end
    end
  end

  describe '#show' do
    context 'when correct id given' do
      before { input_access('1') }

      it 'return finded element of array' do
        expect { subject.show }.to output("Please enter id of comment(begin from 0 to 2): \n1.max payne\n").to_stdout
      end
    end

    context 'when given correct but negative number' do
      before { allow_any_instance_of(Kernel).to receive(:gets).and_return('-1', '1') }

      it 'return finded element of array' do
        expect do
          subject.show
        end.to output("Please enter id of comment(begin from 0 to 2): \nInput error!negative index!enter indeces from 0 to 2: \nPlease enter id of comment(begin from 0 to 2): \n1.max payne\n").to_stdout
      end
    end

    context 'when given index that not exist in array' do
      before { allow_any_instance_of(Kernel).to receive(:gets).and_return('100', '1') }

      it 'raise input error reask input and return correct answer' do
        expect do
          subject.show
        end.to output("Please enter id of comment(begin from 0 to 2): \nInput error!index not exist!enter indeces from 0 to 2: \nPlease enter id of comment(begin from 0 to 2): \n1.max payne\n").to_stdout
      end
    end

    context 'when given not number' do
      before { allow_any_instance_of(Kernel).to receive(:gets).and_return('dou!', '1') }

      it 'raise input error reask input and return correct answer' do
        expect do
          subject.show
        end.to output("Please enter id of comment(begin from 0 to 2): \nInput error! Please enter only numbers(begin from 0 to 2): \nPlease enter id of comment(begin from 0 to 2): \n1.max payne\n").to_stdout
      end
    end

    context 'when given incorrect negative number' do
      before { allow_any_instance_of(Kernel).to receive(:gets).and_return('-100', '1') }

      it 'raise input error reask input and return correct answer' do
        expect do
          subject.show
        end.to output("Please enter id of comment(begin from 0 to 2): \nInput error!index not exist!enter indeces from 0 to 2: \nPlease enter id of comment(begin from 0 to 2): \n1.max payne\n").to_stdout
      end
    end

    context 'when q given' do
      before { input_access('q') }

      it 'exit from show action' do
        expect { subject.show }.to output("Please enter id of comment(begin from 0 to 2): \n").to_stdout
      end
    end
  end

  describe '#create' do
    context 'when given a string to create comment' do
      before { input_access('Game of the year') }

      it 'create new comment and add it to array of comments' do
        expect { subject.create }.to output("Please enter text of comment: \n3.Game of the year\n").to_stdout
      end
    end

    context 'when q given' do
      before { input_access('q') }

      it 'exit from show action' do
        expect { subject.create }.to output("Please enter text of comment: \n").to_stdout
      end
    end
  end

  describe '#update' do
    context 'when given correct id and text' do
      before { allow_any_instance_of(Kernel).to receive(:gets).and_return('0', 'Worst Game of the year!') }

      it 'update element with new text' do
        expect do
          subject.update
        end.to output("Please enter id of comment to update(begin from 0 to 2): \nPlease enter text that update chosen id: \n0.Worst Game of the year!\n").to_stdout
      end
    end

    context 'when given negative id and text' do
      before { allow_any_instance_of(Kernel).to receive(:gets).and_return('-1', 'Worst Game of the year!') }

      it 'update element with new text' do
        expect do
          subject.update
        end.to output("Please enter id of comment to update(begin from 0 to 2): \nPlease enter text that update chosen id: \n2.Worst Game of the year!\n").to_stdout
      end
    end

    context 'when given index that not exist in array' do
      before { allow_any_instance_of(Kernel).to receive(:gets).and_return('100', '0', 'Worst Game of the year!') }

      it 'raise input error reask input and and executes correctly' do
        expect do
          subject.update
        end.to output("Please enter id of comment to update(begin from 0 to 2): \nInput error!index not exist!enter indeces from 0 to 2: \nPlease enter id of comment to update(begin from 0 to 2): \nPlease enter text that update chosen id: \n0.Worst Game of the year!\n").to_stdout
      end
    end

    context 'when not number given for id' do
      before { allow_any_instance_of(Kernel).to receive(:gets).and_return('dou!', '1', 'q') }

      it 'raise input error reask input and executes correctly' do
        expect do
          subject.update
        end.to output("Please enter id of comment to update(begin from 0 to 2): \nInput error! Please enter only numbers(begin from 0 to 2): \nPlease enter id of comment to update(begin from 0 to 2): \nPlease enter text that update chosen id: \n").to_stdout
      end
    end

    context 'when q given for id input' do
      before { input_access('q') }

      it 'exit from show action' do
        expect { subject.update }.to output("Please enter id of comment to update(begin from 0 to 2): \n").to_stdout
      end
    end

    context 'when q given for text input' do
      before { allow_any_instance_of(Kernel).to receive(:gets).and_return('0', 'q') }

      it 'exit from show action' do
        expect do
          subject.update
        end.to output("Please enter id of comment to update(begin from 0 to 2): \nPlease enter text that update chosen id: \n").to_stdout
      end
    end
  end

  describe '#destroy' do
    context 'when correct id given' do
      before { input_access('0') }

      it 'delete chosen comment from array' do
        expect { subject.destroy }.to output("Please enter id of comment to delete(begin from 0 to 2): \n").to_stdout
        expect(subject.index).to eq(['max payne', 'doom'])
        expect { subject.index }.to output("0.max payne\n1.doom\n").to_stdout
      end
    end

    context 'when not number given for id' do
      before { allow_any_instance_of(Kernel).to receive(:gets).and_return('dou!', '1') }

      it 'raise input error reask input and executes correctly' do
        expect do
          subject.destroy
        end.to output("Please enter id of comment to delete(begin from 0 to 2): \nInput error! Please enter only numbers(begin from 0 to 2): \nPlease enter id of comment to delete(begin from 0 to 2): \n").to_stdout
      end
    end

    context 'when given index that not exist in array' do
      before { allow_any_instance_of(Kernel).to receive(:gets).and_return('100', '0') }

      it 'raise input error reask input and and executes correctly' do
        expect do
          subject.destroy
        end.to output("Please enter id of comment to delete(begin from 0 to 2): \nInput error!index not exist!enter indeces from 0 to 2: \nPlease enter id of comment to delete(begin from 0 to 2): \n").to_stdout
      end
    end

    context 'when q given for id input' do
      before { input_access('q') }

      it 'exit from show action' do
        expect { subject.destroy }.to output("Please enter id of comment to delete(begin from 0 to 2): \n").to_stdout
      end
    end
  end
end
