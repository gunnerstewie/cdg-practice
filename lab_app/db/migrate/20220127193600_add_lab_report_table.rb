# frozen_string_literal: true

class AddLabReportTable < ActiveRecord::Migration[6.1]
  def change
    create_table :lab_reports do |t|
      t.string :title, null: false, limit: 250
      t.string :description, limit: 500
      t.string :grade
      t.references :user, null: false, foreign_key: true
      t.timestamps
    end
  end
end
