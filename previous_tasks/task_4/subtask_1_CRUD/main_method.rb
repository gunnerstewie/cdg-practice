# frozen_string_literal: true

require_relative './main_class'
def main
  inst = Main.new
  puts 'Enter operation you want to execute on file:(index, find, where, update, delete)'
  operation = gets.chomp

  case operation
  when 'index'
    inst.index

  when 'find'
    puts 'enter number of line (1-5):'
    ld = gets.chomp.to_i
    inst.find(ld)

  when 'where'
    puts 'enter pattern to find lines:'
    pattern = gets.chomp
    inst.where(pattern)

  when 'update'
    puts 'enter number of line to update(0-4):'
    ld = gets.chomp.to_i
    puts 'enter text that update line:'
    text = gets.chomp
    inst.update(ld, text)

  when 'delete'
    puts 'enter number of line to delete(0-4):'
    ld = gets.chomp.to_i
    inst.delete(ld)
  end
end
puts main
