# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'lab_reports/new', type: :view do
  before do
    assign(:lab_report, LabReport.new(title: 'Alexey', description: 'Leskov'))
  end

  context 'when render text' do
    it 'render new user form' do
      render

      expect(rendered).to match(/Title/)
      expect(rendered).to match(/Description/)
      expect(rendered).to match(/User/)
      expect(rendered).to match(/Create lab report/)
    end
  end

  context 'when check controller path' do
    it 'infers the controller path' do
      expect(controller.request.path_parameters[:controller]).to eq('lab_reports')
      expect(controller.controller_path).to eq('lab_reports')
    end
  end

  context 'when check controller action' do
    it 'infers the controller action' do
      expect(controller.request.path_parameters[:action]).to eq('new')
    end
  end
end
