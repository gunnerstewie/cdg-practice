# frozen_string_literal: true

require_relative './resource'

class PostsController
  extend Resource

  def initialize
    @posts = ['packman', 'max payne', 'doom']
  end

  def index
    @posts.each_with_index do |post, index|
      puts "#{index}.#{post}"
    end
  end

  def show
    loop do
      puts "Please enter id of post(begin from 0 to #{@posts.length - 1}): "
      id = gets.chomp
      break if id == 'q'

      if id != '0' && id.to_i.zero?
        puts "Input error! Please enter only numbers(begin from 0 to #{@posts.length - 1}): "
        redo
      elsif @posts[id.to_i].nil?
        puts "Input error!index not exist!enter indeces from 0 to #{@posts.length - 1}: "
        redo
      elsif id.to_i.negative?
        puts "Input error!negative index!enter indeces from 0 to #{@posts.length - 1}: "
        redo
      else
        @posts.each_with_index do |post, index|
          puts "#{index}.#{post}" if index == id.to_i
        end
      end
      break
    end
  end

  def create
    puts 'Please enter text of post: '
    text = gets.chomp
    return if text == 'q'

    @posts << text

    @posts.each_with_index do |post, index|
      puts "#{index}.#{post}" if post == text
    end
  end

  def update
    loop do
      puts "Please enter id of post to update(begin from 0 to #{@posts.length - 1}): "
      id = gets.chomp
      break if id == 'q'

      if id != '0' && id.to_i.zero?
        puts "Input error! Please enter only numbers(begin from 0 to #{@posts.length - 1}): "
        redo
      elsif @posts[id.to_i].nil?
        puts "Input error!index not exist!enter indeces from 0 to #{@posts.length - 1}: "
        redo
      else
        puts 'Please enter text that update chosen id: '
        text = gets.chomp
        break if text == 'q'

        @posts[id.to_i] = text
        @posts.each_with_index do |post, index|
          puts "#{index}.#{post}" if post == text
        end
      end

      break
    end
  end

  def destroy
    loop do
      puts "Please enter id of post to delete(begin from 0 to #{@posts.length - 1}): "
      id = gets.chomp
      break if id == 'q'

      if id != '0' && id.to_i.zero?
        puts "Input error! Please enter only numbers(begin from 0 to #{@posts.length - 1}): "
        redo
      elsif @posts[id.to_i].nil?
        puts "Input error!index not exist!enter indeces from 0 to #{@posts.length - 1}: "
        redo
      else
        @posts.delete_at(id.to_i)
      end
      break
    end
  end
end
