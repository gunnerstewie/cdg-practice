# frozen_string_literal: true

require_relative './cash_machine_class'

RSpec.describe CashMachine do
  subject { described_class.new }
  let(:buffer) { 'task_5/subtask_1_cash_machine/buffer.txt' }
  let(:balance) { 'task_5/subtask_1_cash_machine/balance.txt' }

  def money(num_1, num_2)
    allow_any_instance_of(Kernel).to receive(:gets).and_return(num_1, num_2)
    subject.open_buffer
  end

  describe 'init' do
    context 'when balance file do not exist' do
      before do
        File.delete('task_5/subtask_1_cash_machine/balance.txt')
        allow_any_instance_of(Kernel).to receive(:gets).and_return('Q')
      end

      it 'creates balance file with 100.0' do
        expect do
          subject.init
        end.to output("Hello partner! Your balance is: 100.0\nPlease choose operation:\n  D - for deposit,\n  W - for withdraw,\n  B - for return your current balance and\n  Q - for quit program\n  (Input is not case sensitive)\nYour balance is 100.0.Goodbye and have a nice day!\n").to_stdout
      end
    end

    context 'when balance file exist' do
      before do
        File.write('task_5/subtask_1_cash_machine/balance.txt', '200')
        allow_any_instance_of(Kernel).to receive(:gets).and_return('Q')
      end

      it 'read current balance and write it on buffer' do
        expect do
          subject.init
        end.to output("Hello partner! Your balance is: 200\nPlease choose operation:\n  D - for deposit,\n  W - for withdraw,\n  B - for return your current balance and\n  Q - for quit program\n  (Input is not case sensitive)\nYour balance is 200.Goodbye and have a nice day!\n").to_stdout
      end
    end
  end

  describe 'open_buffer' do
    subject { described_class.new.open_buffer }

    context 'at any method call' do
      it { is_expected.to eq(File.foreach(buffer).first.to_i) }
    end
  end

  describe 'deposit' do
    context 'when deposit is > 0' do
      before do
        allow_any_instance_of(Kernel).to receive(:gets).and_return('12')
      end

      it 'return summary of balance and input' do
        expect do
          subject.deposit
        end.to output("Please enter summary to deposit:\n    (summary cannot be less that 0)\nYour current balance is #{File.read(buffer).to_i + 12}\n").to_stdout
      end
    end

    context 'when deposit is  0' do
      before { money('0', '12') }

      it 'return input error, ask for input again then return correct answer' do
        expect do
          subject.deposit
        end.to output("Please enter summary to deposit:\n    (summary cannot be less that 0)\nInvalid input!Please input only positive numbers!:\nYour current balance is #{File.read(buffer).to_i + 12}\n").to_stdout
      end
    end

    context 'when deposit is < 0' do
      before { money('-2', '12') }

      it 'return input error, ask for input again then return correct answer' do
        expect do
          subject.deposit
        end.to output("Please enter summary to deposit:\n    (summary cannot be less that 0)\nInvalid input!Please input only positive numbers!:\nYour current balance is #{File.read(buffer).to_i + 12}\n").to_stdout
      end
    end

    context 'when given not number' do
      before { money('dou!', '12') }

      it 'return input error, ask for input again then return correct answer' do
        expect do
          subject.deposit
        end.to output("Please enter summary to deposit:\n    (summary cannot be less that 0)\nInvalid input!Please input only positive numbers!:\nYour current balance is #{File.read(buffer).to_i + 12}\n").to_stdout
      end
    end
  end

  describe 'withdraw' do
    context 'when withdraw > 0 and <= balance' do
      before do
        allow_any_instance_of(Kernel).to receive(:gets).and_return('12')
        subject.open_buffer
      end
      it 'return subtraction withdraw from balance' do
        expect do
          subject.withdraw
        end.to output("Please enter summary to withdraw:\n        (summary cannot be less than 0 and less or equal to current balance)\nYour current balance is #{File.read(buffer).to_i - 12}\n").to_stdout
      end
    end

    context 'when withdraw > 0 and > balance' do
      before { money('120000', '12') }

      it 'return input error, ask for input again then return correct answer' do
        expect do
          subject.withdraw
        end.to output("Please enter summary to withdraw:\n        (summary cannot be less than 0 and less or equal to current balance)\nInvalid input!.Withdraw summary must be positive number\n        and less or equal to your current balance:\nYour current balance is #{File.read(buffer).to_i - 12}\n").to_stdout
      end
    end

    context 'when withdraw < 0' do
      before { money('-12', '12') }

      it 'return input error, ask for input again then return correct answer' do
        expect do
          subject.withdraw
        end.to output("Please enter summary to withdraw:\n        (summary cannot be less than 0 and less or equal to current balance)\nInvalid input!.Withdraw summary must be positive number\n        and less or equal to your current balance:\nYour current balance is #{File.read(buffer).to_i - 12}\n").to_stdout
      end
    end
  end

  describe 'balance' do
    context 'when its possible to print balance' do
      before { subject.open_buffer }

      it 'return current balance' do
        expect { subject.balance }.to output("Your current balance is #{File.read(buffer)}\n").to_stdout
      end
    end
  end

  describe 'quit' do
    context 'when quit program' do
      before { subject.open_buffer }

      it 'return current balance' do
        expect do
          subject.quit
        end.to output("Your balance is #{File.read(buffer)}.Goodbye and have a nice day!\n").to_stdout
      end
    end
  end
end
