# frozen_string_literal: true

require_relative './string_check'

RSpec.describe '#string_check' do
  describe 'user input' do
    def input_access(word)
      allow_any_instance_of(Kernel).to receive(:gets).and_return(word)
    end
    context 'when word ends on "CS"' do
      before do
        input_access('qwertyCS')
      end

      it 'print 2 power word length' do
        expect(string_check).to eq(2**'qwertyCS'.length)
      end
    end

    context 'when word do not ends on "CS"' do
      before do
        input_access('qwerty')
      end

      it 'print reversed word' do
        expect(string_check).to eq('qwerty'.reverse)
      end
    end
  end
end
