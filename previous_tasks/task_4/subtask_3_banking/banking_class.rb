# frozen_string_literal: true

class Banking
  BALANCE = 'task_4/subtask_3_banking/balance.txt'
  BUFFER = 'task_4/subtask_3_banking/buffer.txt'

  def start
    if File.exist?('task_4/subtask_3_banking/balance.txt')
      @balance = 'task_4/subtask_3_banking/balance.txt'
    else
      @balance = File.open('task_4/subtask_3_banking/balance.txt', 'w')
      @balance.write('100.0')
      @balance.close
    end
    puts "Hello partner! Your balance is: #{File.read(@balance)}"
    File.write(BUFFER, File.read(@balance))
  end

  def open_buffer
    @buffer = File.open(BUFFER, 'r+')
    @cash = File.foreach(BUFFER).first.to_i
  end

  def deposit
    open_buffer
    puts 'Please enter summary to deposit:
    (summary cannot be less that 0)'
    while money = gets.chomp.to_i
      if money.positive?
        @buffer.write((money + @cash).to_s)
        @buffer.close
        puts "Your current balance is #{File.read(BUFFER)}"
        break
      else
        puts 'Invalid input!Please input only positive numbers!:'
      end
    end
  end

  def withdraw
    open_buffer
    puts 'Please enter summary to withdraw:
        (summary cannot be less than 0 and less or equal to current balance)'
    while money = gets.chomp.to_i
      if money.positive? && money <= File.read(@buffer).to_i
        @buffer.write((@cash - money).to_s)
        @buffer.close
        puts "Your current balance is #{File.read(@buffer)}"
        break
      else
        puts 'Invalid input!.Withdraw summary must be positive number
        and less or equal to your current balance:'
      end
    end
  end

  def balance
    open_buffer
    puts "Your current balance is #{File.read(@buffer)}"
  end

  def quit
    File.write(BALANCE, File.read(BUFFER))
    puts "Your balance is #{File.read(BALANCE)}.Goodbye and have a nice day!"
    File.write(BUFFER, '')
  end
end
