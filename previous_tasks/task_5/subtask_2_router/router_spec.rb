# frozen_string_literal: true

require_relative './posts_controller'
require_relative './comments_controller'
require_relative './resource'
require_relative './router'

RSpec.describe Router do
  subject { described_class.new }

  describe '#init' do
    context 'when user input q' do
      before { allow_any_instance_of(Kernel).to receive(:gets).and_return('q') }

      it 'quit program' do
        expect do
          subject.init
        end.to output("Choose resource you want to interact (1 - Posts, 2 - Comments, q - Exit): Good bye!\n").to_stdout
      end
    end

    context 'when user input 1' do
      before { allow_any_instance_of(Kernel).to receive(:gets).and_return('1', 'q') }

      it 'quit program' do
        expect do
          subject.init
        end.to output("Choose resource you want to interact (1 - Posts, 2 - Comments, q - Exit): Choose verb to interact with PostsController (GET/POST/PUT/DELETE) / q to exit: Input Error!Please enter correct action(GET/POST/PUT/DELETE) or q for quit verb choise!\nChoose verb to interact with PostsController (GET/POST/PUT/DELETE) / q to exit: Choose resource you want to interact (1 - Posts, 2 - Comments, q - Exit): Good bye!\n").to_stdout
      end
    end

    context 'when user input 2' do
      before { allow_any_instance_of(Kernel).to receive(:gets).and_return('2', 'q') }

      it 'quit program' do
        expect do
          subject.init
        end.to output("Choose resource you want to interact (1 - Posts, 2 - Comments, q - Exit): Choose verb to interact with CommentsController (GET/POST/PUT/DELETE) / q to exit: Input Error!Please enter correct action(GET/POST/PUT/DELETE) or q for quit verb choise!\nChoose verb to interact with CommentsController (GET/POST/PUT/DELETE) / q to exit: Choose resource you want to interact (1 - Posts, 2 - Comments, q - Exit): Good bye!\n").to_stdout
      end
    end

    context 'when user input incorrect command' do
      before { allow_any_instance_of(Kernel).to receive(:gets).and_return('dou!', 'q') }

      it 'return error, reask input' do
        expect do
          subject.init
        end.to output("Choose resource you want to interact (1 - Posts, 2 - Comments, q - Exit): Input Error! Please enter correct number of resource(1, 2) or q for quit program:\nChoose resource you want to interact (1 - Posts, 2 - Comments, q - Exit): Good bye!\n").to_stdout
      end
    end
  end
end
