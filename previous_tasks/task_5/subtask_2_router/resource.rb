# frozen_string_literal: true

module Resource
  def connection(routes)
    if routes.nil?
      puts "No route matches for #{self}"
      return
    end

    loop do
      print "Choose verb to interact with #{self} (GET/POST/PUT/DELETE) / q to exit: "
      verb = gets.chomp
      break if verb == 'q'

      case verb
      when 'GET'
        loop do
          print 'Choose action (index/show) / q to exit: '
          action = gets.chomp
          case action
          when 'q'
            break
          when 'index', 'show'
            routes[verb][action].call
            break
          else
            puts 'Input Error Please enter correct action(index, show) or q for quit action choise: !'
          end
        end
      when 'POST', 'PUT', 'DELETE'
        routes[verb].call
      else
        puts 'Input Error!Please enter correct action(GET/POST/PUT/DELETE) or q for quit verb choise!'
        redo
      end
    end
  end
end
