# frozen_string_literal: true

require 'rails_helper'

RSpec.describe LabReportPolicy do
  let(:user) { create(:user, role: 'admin') }
  let(:creator) { create(:user) }
  let(:record) { create(:lab_report, user: creator) }
  let(:context) { { user: user } }

  describe_rule :update? do
    describe 'positive case' do
      succeed 'when user role is admin'
      context 'when non admin users in system' do
        let(:record) { create(:lab_report, user: user) }

        succeed 'when user is manager and user is creator of lab report' do
          let(:user) { create(:user, role: 'manager') }
        end
        succeed 'when user is  member and user is creator of lab report' do
          let(:user) { create(:user, role: 'member') }
        end
      end
    end

    describe 'negative case' do
      failed 'when user is manager and user is not creator of lab report' do
        let(:user) { create(:user, role: 'manager') }
      end

      failed 'when user is member and user is not creator of lab report' do
        let(:user) { create(:user, role: 'member') }
      end
    end
  end

  describe 'relation scope' do
    subject { policy.apply_scope(target, type: :active_record_relation) }

    let(:target) do
      LabReport.all
    end

    describe 'positive cases' do
      context 'when admin can see all reports' do
        it { is_expected.to eq(LabReport.all) }
      end

      context 'when manager can see all reports' do
        before { user.update!(role: :manager) }

        it { is_expected.to eq(LabReport.all) }
      end

      context 'when member can see only his reports' do
        let(:record) { create(:lab_report, user: user) }

        before { user.update!(role: :member) }

        it { is_expected.to eq(LabReport.where(user: user)) }
      end
    end

    describe 'negative cases' do
      context 'when member dont see reports that not belongs to him' do
        before { user.update!(role: :member) }

        it { is_expected.to be_empty }
      end
    end
  end
end
