# frozen_string_literal: true

require_relative './foobar'

RSpec.describe '#foobar' do
  describe 'user input' do
    def input_access(num_1, num_2)
      allow_any_instance_of(Kernel).to receive(:gets).and_return(num_1, num_2)
    end

    context 'when one of numbers is equal to 20' do
      before do
        input_access('19', '20')
      end

      it 'print second number' do
        expect(foobar).to eq('20')
      end
    end

    context 'when any numbers is equal to 20' do
      before do
        input_access('19', '19')
      end

      it 'return summary of numbers' do
        expect(foobar).to eq('38')
      end
    end
  end
end
