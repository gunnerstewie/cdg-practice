# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'DELETE /lab_reports/:id', type: :request do
  let(:path) { "/lab_reports/#{lab_report.id}" }

  let(:user) { create(:user) }
  let(:lab_report) { create(:lab_report, user_id: user.id) }

  describe 'when user logged in' do
    before { sign_in user }

    context 'when delete lab_report' do
      it 'successful' do
        delete path
        expect(response).to have_http_status :found
        expect(response).to redirect_to(lab_reports_path)
        expect(flash[:success]).to match(/Lab report has been destroyed!/)
        expect(LabReport.count).to eq(0)
      end
    end

    context 'when wrong id given' do
      let(:path) { '/lab_reports/666' }

      it 'record not found and redirect' do
        delete path
        expect(response).to redirect_to(lab_reports_path)
        expect(flash[:danger]).to match(/Record not found/)
      end
    end
  end

  context 'when user not logged in' do
    it 'redirect to log in path' do
      delete path
      expect(response).to redirect_to(new_user_session_path)
    end
  end
end
