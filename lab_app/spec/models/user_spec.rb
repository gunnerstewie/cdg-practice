# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User, type: :model do
  it { is_expected.to have_many(:lab_reports) }
  it { is_expected.to allow_value('index@gmail.com').for(:email) }
  it { is_expected.not_to allow_value('wrong').for(:email) }
  it { is_expected.to validate_length_of(:first_name).is_at_least(1).is_at_most(100) }
end
