# frozen_string_literal: true

require_relative './banking_class'

def banking
  banking = Banking.new
  banking.start
  puts text = 'Please choose operation:
  D - for deposit,
  W - for withdraw,
  B - for return your current balance and
  Q - for quit program
  (Input is not case sensitive)
  ------------------------------------------'
  loop do
    operation = gets.chomp.capitalize
    case operation
    when 'D'
      banking.deposit
    when 'W'
      banking.withdraw
    when 'B'
      banking.balance
    when 'Q'
      banking.quit
      break
    else
      puts 'Invalid input!Please input correct operations! (d, w, b, q)
      --------------------------------'
    end
    puts text
  end
end
banking
