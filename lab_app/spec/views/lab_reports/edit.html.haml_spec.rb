# frozen_string_literal: true

require 'rails_helper'

RSpec.describe '/lab_reports/edit', type: :view do
  context 'when render edit page' do
    let!(:user) { create(:user) }

    before do
      @lab_report = assign(:lab_report, LabReport.create!(
        title: 'MyString',
        user_id: user.id
      ))
    end

    it 'renders the edit lab_report form' do
      render
      expect(rendered).to match(/Grade/)
      expect(rendered).to match(/Mark/)
    end
  end

  context 'when check controller path' do
    it 'infers the controller path' do
      expect(controller.request.path_parameters[:controller]).to eq('/lab_reports')
      expect(controller.controller_path).to eq('/lab_reports')
    end
  end

  context 'when check controller action' do
    it 'infers the controller action' do
      expect(controller.request.path_parameters[:action]).to eq('edit')
    end
  end
end
