# frozen_string_literal: true

def greeting
  puts 'Please enter your first name:'
  first_name = gets.chomp
  puts 'Please enter your last name:'
  last_name = gets.chomp
  puts 'Please enter your age:'
  age = gets.chomp.to_i

  if age < 18
    "Привет, #{first_name} #{last_name}. Тебе меньше 18 лет, но начать учиться программировать никогда не рано"
  else
    "Привет, #{first_name} #{last_name}. Самое время заняться делом!"
  end
end
puts greeting
