# frozen_string_literal: true

require_relative './main_class'

RSpec.describe Main do
  subject { described_class.new }
  let(:text) { 'task_4/subtask_1_CRUD/text.txt' }

  describe 'index' do
    before do
      File.write(text, "Firstline\nSecondline\nThirdline\nFourthline\nFifthline")
    end

    context 'reading text file' do
      it 'return text written on file' do
        expect(subject.index).to eq(File.read(text))
      end
    end
  end

  describe 'find' do
    context 'find line by index' do
      it 'puts founded line even its empty' do
        expect { subject.find(1) }.to output("Firstline\n").to_stdout
      end
    end

    context 'file do not have line with index 0' do
      it 'puts nothing' do
        expect { subject.find(0) }.to output('').to_stdout
      end
    end
  end

  describe 'where' do
    context 'find and return line by pattern' do
      it 'puts line founded by pattern' do
        expect { subject.where('Second') }.to output("Secondline\n").to_stdout
      end
    end

    context 'find and return all lines that contain pattern' do
      it 'puts lines founded by pattern' do
        expect { subject.where('line') }.to output("#{File.read(text)}\n").to_stdout
      end
    end

    context 'return nothing when no line founded by pattern' do
      it 'puts nothing' do
        expect { subject.where('Godzilla') }.to output('').to_stdout
      end
    end
  end

  describe 'update' do
    context 'find line in file by index and update it by new text' do
      before { subject.update(1, 'Updated text') }

      it 'update line in file' do
        expect do
          puts File.read(text)
        end.to output("Firstline\nUpdated text\nThirdline\nFourthline\nFifthline\n").to_stdout
      end
    end

    context 'when given line without index' do
      before do
        File.write(text, "Firstline\nSecondline\nThirdline\nFourthline\nFifthline")
        subject.update(10, 'Updated text')
      end
      it 'update line in file' do
        expect { puts File.read(text) }.to output("Firstline\nSecondline\nThirdline\nFourthline\nFifthline\n").to_stdout
      end
    end
  end

  describe 'delete' do
    context 'find line and delete it' do
      before do
        File.write(text, "Firstline\nSecondline\nThirdline\nFourthline\nFifthline")
        subject.delete(0)
      end

      it 'delete line and delete empty line after deleting' do
        expect { puts File.read(text) }.to output("Secondline\nThirdline\nFourthline\nFifthline\n").to_stdout
        expect(File.open(text).readlines.size).to eq(4)
      end
    end

    context 'when given line without index' do
      before do
        File.write(text, "Firstline\nSecondline\nThirdline\nFourthline\nFifthline")
        subject.delete(10)
      end

      it 'do not delete anything' do
        expect { puts File.read(text) }.to output("Firstline\nSecondline\nThirdline\nFourthline\nFifthline\n").to_stdout
        expect(File.open(text).readlines.size).to eq(5)
      end
    end
  end
end
