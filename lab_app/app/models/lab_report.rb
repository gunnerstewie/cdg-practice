# frozen_string_literal: true

class LabReport < ApplicationRecord
  belongs_to :user
  validates :title, length: { presence: true, minimum: 1, maximum: 250, allow_blank: false }
  validates :description, length: { maximum: 500 }
  accepts_nested_attributes_for :user, allow_destroy: true
end
