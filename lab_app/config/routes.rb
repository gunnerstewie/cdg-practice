# frozen_string_literal: true

Rails.application.routes.draw do
  root 'lab_reports#index'
  devise_for :users, controllers: {
    registrations: 'users/registrations',
    sessions: 'users/sessions'
  }
  resources :lab_reports, except: :show
end
