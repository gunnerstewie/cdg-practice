# frozen_string_literal: true

require_relative './banking_class'

RSpec.describe Banking do
  subject { described_class.new }
  let(:buffer) { 'task_4/subtask_3_banking/buffer.txt' }
  let(:balance) { 'task_4/subtask_3_banking/balance.txt' }

  def money(num_1, num_2)
    allow_any_instance_of(Kernel).to receive(:gets).and_return(num_1, num_2)
    subject.open_buffer
  end

  describe 'start' do
    context 'when balance file do not exist' do
      before do
        File.delete('task_4/subtask_3_banking/balance.txt')
        subject.start
      end

      it 'creates balance file with 100.0' do
        expect { subject.start }.to output("Hello partner! Your balance is: 100.0\n").to_stdout
      end
    end
    context 'when balance file do not exist' do
      before { File.write('task_4/subtask_3_banking/balance.txt', '200') }

      it 'read current balance and write it on buffer' do
        expect { subject.start }.to output("Hello partner! Your balance is: 200\n").to_stdout
      end
    end
  end

  describe 'open_buffer' do
    subject { described_class.new.open_buffer }

    context 'at any method call' do
      it { is_expected.to eq(File.foreach(buffer).first.to_i) }
    end
  end

  describe 'deposit' do
    context 'when deposit is > 0' do
      before do
        allow_any_instance_of(Kernel).to receive(:gets).and_return('12')
        subject.open_buffer
      end

      it 'return summary of balance and input' do
        expect do
          subject.deposit
        end.to output("Please enter summary to deposit:\n    (summary cannot be less that 0)\nYour current balance is #{File.read(buffer).to_i + 12}\n").to_stdout
      end
    end

    context 'when deposit is  0' do
      before { money('0', '12') }

      it 'return input error, ask for input again then return correct answer' do
        expect do
          subject.deposit
        end.to output("Please enter summary to deposit:\n    (summary cannot be less that 0)\nInvalid input!Please input only positive numbers!:\nYour current balance is #{File.read(buffer).to_i + 12}\n").to_stdout
      end
    end

    context 'when deposit is < 0' do
      before { money('-2', '12') }

      it 'return input error, ask for input again then return correct answer' do
        expect do
          subject.deposit
        end.to output("Please enter summary to deposit:\n    (summary cannot be less that 0)\nInvalid input!Please input only positive numbers!:\nYour current balance is #{File.read(buffer).to_i + 12}\n").to_stdout
      end
    end

    context 'when given not number' do
      before { money('dou!', '12') }

      it 'return input error, ask for input again then return correct answer' do
        expect do
          subject.deposit
        end.to output("Please enter summary to deposit:\n    (summary cannot be less that 0)\nInvalid input!Please input only positive numbers!:\nYour current balance is #{File.read(buffer).to_i + 12}\n").to_stdout
      end
    end
  end

  describe 'withdraw' do
    context 'when withdraw > 0 and <= balance' do
      before do
        allow_any_instance_of(Kernel).to receive(:gets).and_return('12')
        subject.open_buffer
      end
      it 'return subtraction withdraw from balance' do
        expect do
          subject.withdraw
        end.to output("Please enter summary to withdraw:\n        (summary cannot be less than 0 and less or equal to current balance)\nYour current balance is #{File.read(buffer).to_i - 12}\n").to_stdout
      end
    end

    context 'when withdraw > 0 and > balance' do
      before { money('120000', '12') }

      it 'return input error, ask for input again then return correct answer' do
        expect do
          subject.withdraw
        end.to output("Please enter summary to withdraw:\n        (summary cannot be less than 0 and less or equal to current balance)\nInvalid input!.Withdraw summary must be positive number\n        and less or equal to your current balance:\nYour current balance is #{File.read(buffer).to_i - 12}\n").to_stdout
      end
    end

    context 'when withdraw < 0' do
      before { money('-12', '12') }

      it 'return input error, ask for input again then return correct answer' do
        expect do
          subject.withdraw
        end.to output("Please enter summary to withdraw:\n        (summary cannot be less than 0 and less or equal to current balance)\nInvalid input!.Withdraw summary must be positive number\n        and less or equal to your current balance:\nYour current balance is #{File.read(buffer).to_i - 12}\n").to_stdout
      end
    end

    context 'when given not number' do
      before { money('dou!', '12') }

      it 'return input error, ask for input again then return correct answer' do
        expect do
          subject.withdraw
        end.to output("Please enter summary to withdraw:\n        (summary cannot be less than 0 and less or equal to current balance)\nInvalid input!.Withdraw summary must be positive number\n        and less or equal to your current balance:\nYour current balance is #{File.read(buffer).to_i - 12}\n").to_stdout
      end
    end
  end

  describe 'balance' do
    context 'when its possible to print balance' do
      before { subject.open_buffer }

      it 'return current balance' do
        expect { subject.balance }.to output("Your current balance is #{File.read(buffer)}\n").to_stdout
      end
    end
  end

  describe 'quit' do
    context 'when quit program' do
      before { subject.open_buffer }

      it 'return current balance' do
        expect do
          subject.quit
        end.to output("Your balance is #{File.read(buffer)}.Goodbye and have a nice day!\n").to_stdout
      end
    end
  end
end
