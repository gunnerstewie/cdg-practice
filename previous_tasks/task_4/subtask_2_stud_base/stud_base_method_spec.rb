# frozen_string_literal: true

require_relative './stud_base_method'

RSpec.describe '#stud_base' do
  let(:list) { 'task_4/subtask_2_stud_base/stud_base_list.txt' }
  let(:results) { 'task_4/subtask_2_stud_base/results.txt' }

  describe 'user input' do
    context 'when input -1' do
      before do
        allow_any_instance_of(Kernel).to receive(:gets).and_return('-1')
      end

      it 'end program' do
        expect { stud_base }.to output("Enter age for search:\nprogram is over\n\n").to_stdout
        expect(File.read(results)).to eq('')
      end
    end

    context 'when input all ages from list' do
      before do
        allow_any_instance_of(Kernel).to receive(:gets).and_return('1', '2', '3', '4', '5', '6', '7', '8', '9', '0')
      end

      it 'end program' do
        expect do
          stud_base
        end.to output("Enter age for search:\n#{"Enter age for search again please:\n" * 10}program is over\n#{File.read(list)}\n").to_stdout
        expect(File.read(results)).to eq('')
      end
    end

    context 'when user do not input full list of studetnts' do
      before do
        allow_any_instance_of(Kernel).to receive(:gets).and_return('1', '1', '1', '-1')
      end
      it 'cycle asks for enter age again until cycle not escape' do
        expect do
          stud_base
        end.to output("Enter age for search:\n#{"Enter age for search again please:\n" * 3}program is over\n#{File.open(list).first * 3}").to_stdout
      end
    end
  end
end
